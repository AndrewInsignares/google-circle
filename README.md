File Contents:

    index.html - an html/javascript file containing all logic 
    
Requirements:

    Web Browser w/ Javascript Enabled

View on S3:

	1) Visit url in browser: 
		
		http://google-circle.s3-website-us-east-1.amazonaws.com/

View Locally:

	1) Clone this git repository locally:

		$: git clone https://gitlab.com/AndrewInsignares/google-circle.git

	2) Open file in browser 

		[current_path]/google-circle/index.html
